package gourban;

import com.stripe.exception.StripeException;

import java.io.Console;
import java.util.InputMismatchException;

public interface Main {

    static void main(String[] args) {
        Console console = System.console();
        ChargeProcess chargeProcess = new ChargeProcess("sk_test_4eC39HqLyjWDarjtT1zdp7dc", console);
        try {
            chargeProcess.charge();
        } catch (InputMismatchException e) {
            console.writer().println(e.getMessage());
        } catch (StripeException e) {
            console.writer().println("Ooops, an error occurred during the process with our payment provider. " +
                    "Please, contact our customer service.");
        }
    }
}
