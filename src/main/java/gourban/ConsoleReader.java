package gourban;

import java.io.Console;
import java.util.InputMismatchException;
import java.util.regex.Pattern;

class ConsoleReader {

    /**
     * Boggy email validation pattern not matching RFC https://tools.ietf.org/html/rfc2822 (the RFC is way too wide)
     */
    private static final Pattern EMAIL_PATTERN = Pattern.compile("[\\w0-9+._-]+@[\\w0-9+._-]+\\.[\\w0-9+._-]+");

    private static final Pattern DOUBLE_PATTERN = Pattern.compile("[\\d]+(\\.[\\d]+)?");

    private final Console console;

    ConsoleReader(Console console) {
        this.console = console;
    }

    /**
     * Reads a value from the console.
     *
     * @param message           the message to display in the console to request the end-user to enter a value
     * @param validationPattern the pattern to validate the value read against
     * @param errorPrefix       the error prefix to include in the exception in case the read value does not match the pattern
     * @return the value read
     * @throws InputMismatchException if the value read does not match the pattern
     */
    String readLine(String message, Pattern validationPattern, String errorPrefix) {
        String value = console.readLine(message);
        if (validationPattern.matcher(value).matches()) {
            return value;
        }
        throw new InputMismatchException(errorPrefix + ". '" + value + "' does not match pattern " + validationPattern);
    }

    /**
     * Reads an email from the console.
     *
     * @param message the message to display in the console to request the end-user to enter a value
     * @return the email read
     * @throws InputMismatchException if the value read is not a valid email
     */
    String readEmail(String message) {
        return readLine(message, EMAIL_PATTERN, "Invalid email address");
    }

    /**
     * Reads a number from the console.
     *
     * @param message the message to display in the console to request the end-user to enter a value
     * @return the number read
     * @throws InputMismatchException if the value read is not a valid number
     */
    double readDouble(String message) {
        return Double.parseDouble(readLine(message, DOUBLE_PATTERN, "Invalid number"));
    }
}
