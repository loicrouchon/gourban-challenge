package gourban;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Refund;
import com.stripe.model.Source;
import com.stripe.param.ChargeCreateParams;
import com.stripe.param.CustomerCreateParams;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.SourceCreateParams;

import java.io.Console;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.regex.Pattern;

class ChargeProcess {

    private enum Action {
        CAPTURE, REFUND;
    }

    private static final Pattern ACTION_PATTERN = Pattern.compile("capture|refund");

    private final ConsoleReader reader;

    private final PrintWriter writer;

    ChargeProcess(String stripeApiKey, Console console) {
        Stripe.apiKey = stripeApiKey;
        reader = new ConsoleReader(console);
        writer = console.writer();
    }

    /**
     * Charges or refund a customer on Stripe.
     * The customer email, amount of the charge and action (capture or refund) is read from the console.
     * In case of capture failure, a refund is attempted.
     *
     * @throws InputMismatchException in case of invalid user input in the console.
     * @throws StripeException        in case of error while interacting with the Stripe payment provider
     */
    void charge() throws StripeException {
        String email = reader.readEmail("> Please enter your email: ");

        writer.println("Creating customer for email " + email + " on Stripe");
        Customer customer = createCustomer(email);

        writer.println("Creating source for customer " + customer.getId() + " on Stripe");
        Source source = createSource("card", "tok_at");

        writer.println("Set source " + source.getId() + " as default for customer " + customer.getId());
        customer.setDefaultSource(source.getId());

        long amount = (long) (reader.readDouble("> Amount to charge in euros: ") * 100L);
        String currency = "EUR";
        writer.println("Creating charge for source " + source.getId() + " with amount " + (double) amount / 100 + " "
                + currency + " on Stripe");
        Charge charge = createChargeWithoutCapture(source, amount, currency);
        try {
            Action action = Action.valueOf(reader.readLine(
                    "> Would you like to capture or to refund the charge " + charge.getId() + "? [capture, refund]",
                    ACTION_PATTERN,
                    "Invalid action")
                    .toUpperCase());
            executeAction(charge, action);
        } catch (IllegalArgumentException e) {
            refund(charge, "Unknown action requested, refund will be performed");
        }
    }

    private static Customer createCustomer(String email) throws StripeException {
        CustomerCreateParams customerParams = CustomerCreateParams.builder().setEmail(email).build();
        return Customer.create(customerParams);
    }

    private static Source createSource(String type, String token) throws StripeException {
        SourceCreateParams sourceParams = SourceCreateParams.builder()
                .setType(type)
                .setToken(token)
                .build();
        return Source.create(sourceParams);
    }

    private static Charge createChargeWithoutCapture(Source source, long amount, String currency) throws StripeException {
        ChargeCreateParams chargeParams = ChargeCreateParams.builder()
                .setAmount(amount)
                .setCurrency(currency)
                .setSource(source.getId())
                .setCapture(false)
                .build();
        return Charge.create(chargeParams);
    }

    private void executeAction(Charge charge, Action action) throws StripeException {
        switch (action) {
            case CAPTURE:
                capture(charge);
                break;
            case REFUND:
                refund(charge, "Performing the refund for charge " + charge.getId());
                break;
            default:
                throw new UnsupportedOperationException("Unknown action type: " + action);
        }
    }

    private void capture(Charge charge) throws StripeException {
        writer.println("Performing the capture for charge " + charge.getId());
        try {
            Charge capture = charge.capture();
            writer.println("Capture " + capture.getId() + " performed");
        } catch (StripeException e) {
            refund(charge, "An exception occurred during the capture " + charge.getId()
                    + ". A Refund will be attempted.");
        }
    }

    private void refund(Charge charge, String reason) throws StripeException {
        writer.println(reason);
        try {
            Refund refund = createRefund(charge);
            writer.println("Refund " + refund.getId() + " performed");
        } catch (StripeException e) {
            writer.println("An error occurred during the refund.");
            throw e;
        }
    }

    private static Refund createRefund(Charge charge) throws StripeException {
        RefundCreateParams refundParams = RefundCreateParams.builder()
                .setCharge(charge.getId())
                .build();
        return Refund.create(refundParams);
    }
}
