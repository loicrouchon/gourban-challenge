# Build the application

`./gradlew installDist`

# Run the application

`./build/install/gourban/bin/gourban`

_A .bat launcher is also available for windows_
